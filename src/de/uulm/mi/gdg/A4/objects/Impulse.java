package de.uulm.mi.gdg.A4.objects;

import processing.core.PApplet;

/**
 * @author Michael Scheurer, Philipp Ripper, Simon Ripper
 */
public class Impulse {

    public float strength;
    public int color;
    public long duration;
    public long delay;
    public long fadeIn;
    public long fadeOut;
    public long elementDuration;
    public long timeStamp;

    public Impulse(float strength, int color, long duration, long delay, long fadeIn, long fadeOut) {
        this.strength = strength;
        this.duration = duration;
        this.delay = delay;
        this.fadeIn = fadeIn;
        this.fadeOut = fadeOut;
        this.color = color;
        this.timeStamp = System.currentTimeMillis();

        long amountOfTransfers =(long)Math.ceil((duration/delay) - 1);
        this.elementDuration = delay*6;
    }

    public float getStrength() {

        long age = System.currentTimeMillis() - timeStamp;

        if(age < fadeIn) {
            return strength/(float)fadeIn*age;
        } else if(age > duration) {
            return 0;
        } else if (age > duration-fadeOut) {
            return strength - (strength/fadeOut*(age-(duration-fadeOut)));
        }

        return strength;
    }

    public float getElementStrength(long timeStamp) {
        float strength = getStrength();
        long age = System.currentTimeMillis() - timeStamp;

        if(age < 2*delay) {
            return (strength/(2*delay))*age;
        } else if (age > elementDuration) {
            return 0;
        } else if (age > elementDuration-(2*delay)) {
            return strength - (strength/(2*delay)*(age-(elementDuration-2*delay)));
        }

        return strength;
    }

}
