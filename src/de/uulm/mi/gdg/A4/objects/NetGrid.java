package de.uulm.mi.gdg.A4.objects;

import processing.core.PApplet;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael Scheurer, Philipp Ripper, Simon Ripper
 */
public class NetGrid extends Grid{

    private int numberOfLevels;	//Anzahl der Ringe
    int numberOfSectors;	//Anzahl der Streben des Netzes (muss >= 3 sein)
	
    private int middleX;	//Mittelpunkt
    private int middleY;
    private double factor = 3.5;
    private int length = 360;
    private int xLength = (int) (length / (numberOfLevels/factor));
    
    private List<NetGridElement> elements;
    private List<Point> origin;
    private List<Point> neworigin;
    
    
    public NetGrid(PApplet parent, ImpulseSpreader impulseSpreader, int levels, int sectors, int midPointX, int midPointY) {
    	
    	numberOfLevels = levels;
    	numberOfSectors = sectors;
    	
    	middleX = midPointX;
        middleY = midPointY;
        factor = 3.5;
        length = 360;
        xLength = (int) (length / (numberOfLevels/factor));
    	
        elements = new ArrayList<>();
        origin = new ArrayList<>();
        neworigin = new ArrayList<>();
        
        //Ursprungspunkt der Streben nach au�en
        for(int i=0; i<numberOfSectors; i++) {
        	origin.add(new Point(middleX, middleY));
        }
        
        for (int i = 1; i <= numberOfLevels; i++){
            //erstellt Eckpunkte der Elemente
        	for(int h = 0; h<numberOfSectors; h++) {
        		neworigin.add(new Point(
        				(int) (middleX + xLength * i * Math.sin(2*Math.PI/numberOfSectors*h)), 
        				(int) (middleY - xLength * i * Math.cos(2*Math.PI/numberOfSectors*h))
        				));
        	}
        	//erstellt Elemente nach au�en und von da im Uhrzeigersinn zur n�chsten Strebe
        	for(int j = 0; j<numberOfSectors; j++) {
            	elements.add(new NetGridElement(parent, impulseSpreader, 
            			origin.get(j).x, 
            			origin.get(j).y, 
            			neworigin.get(j).x, 
            			neworigin.get(j).y,
            			parent.createShape()));
            	if(j==numberOfSectors-1) {
            		elements.add(new NetGridElement(parent, impulseSpreader, 
                			neworigin.get(j).x, 
                			neworigin.get(j).y, 
                			neworigin.get(0).x, 
                			neworigin.get(0).y,
                            parent.createShape()
                			));
            	}else {
            		elements.add(new NetGridElement(parent, impulseSpreader, 
                			neworigin.get(j).x, 
                			neworigin.get(j).y, 
                			neworigin.get(j+1).x, 
                			neworigin.get(j+1).y,
                            parent.createShape()
                			));
            	}
            }
            origin = neworigin;
            neworigin = new ArrayList<>();
        }

        //add Neighbours
        for (NetGridElement ge : elements){
        	for (NetGridElement possibleNeighbourGE : elements){
            	if(!ge.equals(possibleNeighbourGE)){
            		if(ge.getxStart() == possibleNeighbourGE.getxStart() && ge.getyStart() == possibleNeighbourGE.getyStart() ||
                            ge.getxStart() == possibleNeighbourGE.getxEnd() && ge.getyStart() == possibleNeighbourGE.getyEnd() ||
                            ge.getxEnd() == possibleNeighbourGE.getxStart() && ge.getyEnd() == possibleNeighbourGE.getyStart() ||
                            ge.getxEnd() == possibleNeighbourGE.getxEnd() && ge.getyEnd() == possibleNeighbourGE.getyEnd()){
            			ge.addNeighbour(possibleNeighbourGE);
            		}
            	}
            }
        }
    }
    


    @Override
    public void update() {
        for(NetGridElement ge:elements) {
            ge.update();
        }
    }

    @Override
    public void draw() {
        for(NetGridElement ge:elements) {
            ge.draw();
        }
    }

    @Override
    public void impulse(String gridElementId, Impulse impulse) {
        if(gridElementId.equals("a1")) {
        	elements.get(5).addImpulse(impulse);
        } else if(gridElementId.equals("b1")) {
        	elements.get(15).addImpulse(impulse);
        } else if(gridElementId.equals("c1")) {
        	elements.get(25).addImpulse(impulse);
        } else if(gridElementId.equals("d1")) {
        	elements.get(35).addImpulse(impulse);
        } else if(gridElementId.equals("e1")) {
        	elements.get(45).addImpulse(impulse);
        }
        
        switch(gridElementId) {
        case "g1":
            elements.get(0).addImpulse(impulse);
            break;
        case "d2":
            elements.get(5).addImpulse(impulse);
            break;
        case "e2":
            elements.get(10).addImpulse(impulse);
            break;
        case "f#2":
            elements.get(15).addImpulse(impulse);
            break;
        case "g2":
            elements.get(20).addImpulse(impulse);
            break;
        case "a2":
            elements.get(25).addImpulse(impulse);
            break;
        case "b2":
        	elements.get(30).addImpulse(impulse);
            break;
        case "c3":
            elements.get(35).addImpulse(impulse);
            break;
        case "d3":
            elements.get(40).addImpulse(impulse);
            break;
        case "e3":
            elements.get(45).addImpulse(impulse);
            break;
        case "f#3":
            elements.get(50).addImpulse(impulse);
        	break;
        case "g3":
            elements.get(55).addImpulse(impulse);
            break;
        case "a3":
            elements.get(60).addImpulse(impulse);
            break;
        case "b3":
            elements.get(65).addImpulse(impulse);
            break;
        case "d4":
            elements.get(70).addImpulse(impulse);
            break;
        case "e4":
            elements.get(75).addImpulse(impulse);
            break;
        case "g4":
            elements.get(80).addImpulse(impulse);
            break;
        case "d5":
            elements.get(85).addImpulse(impulse);
            break;
        }
    }
}
