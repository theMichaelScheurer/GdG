package de.uulm.mi.gdg.A4.objects;

import processing.core.PApplet;
import processing.core.PShape;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Michael Scheurer
 */
public abstract class GridElement {

    protected PApplet parent;
    protected PShape shape;

    protected List<GridElement> neighbours;
    protected List<Impulse> impulses;
    protected List<Impulse> impulsesToBeSpread;
    protected HashMap<Impulse, Long> impulseTimestamps;
    private ImpulseSpreader impulseSpreader;
    private long timeStamp;

    public abstract void draw();

    public void addNeighbour(GridElement gridElement) {
        neighbours.add(gridElement);
    }

    public void addImpulse(Impulse impulse) {
        impulses.add(impulse);
        impulsesToBeSpread.add(impulse);
        impulseTimestamps.put(impulse, System.currentTimeMillis());
    }

    public void update() {
        long passedTime = System.currentTimeMillis() - timeStamp;
        timeStamp = System.currentTimeMillis();

        List<Impulse> toBeRemoved = new ArrayList<>();
        for(Impulse i : impulsesToBeSpread) {
            if(System.currentTimeMillis() > impulseTimestamps.get(i)+i.delay) {
                for(GridElement ge:neighbours) {
                    if(!ge.impulses.contains(i) && i.getStrength() > 0) {
                        impulseSpreader.accept(i, ge, this);
                    }
                }
                toBeRemoved.add(i);
            }
        }
        impulsesToBeSpread.removeAll(toBeRemoved);

        toBeRemoved = new ArrayList<>();
        for(Impulse i:impulses) {
            if(System.currentTimeMillis() > impulseTimestamps.get(i)+i.duration) {
                toBeRemoved.add(i);
            }
        }
        impulses.removeAll(toBeRemoved);
        for(Impulse i: toBeRemoved) {
            impulseTimestamps.remove(i);
        }

    }

    public float getStrength() {
        float strength = 0;
        for(Impulse i : impulses) {
            strength = i.getElementStrength(impulseTimestamps.get(i)) > strength ? i.getElementStrength(impulseTimestamps.get(i)) : strength;
        }
        return strength;
    }

    public int getColor() {
        int color = 0;

        if(getStrength() <= 0)
            return color;

        float strengthSum = 0;

        for(Impulse i : impulses) {
            if(i.getElementStrength(impulseTimestamps.get(i)) > 0)
                strengthSum += i.getElementStrength(impulseTimestamps.get(i));
        }

        int r = 0;
        int g = 0;
        int b = 0;

        for(Impulse i : impulses) {
            if(i.getElementStrength(impulseTimestamps.get(i)) > 0) {
                r += parent.red(i.color) * parent.red(i.color) * i.getElementStrength(impulseTimestamps.get(i));
                g += parent.green(i.color) * parent.green(i.color) * i.getElementStrength(impulseTimestamps.get(i));
                b += parent.blue(i.color) * parent.blue(i.color) * i.getElementStrength(impulseTimestamps.get(i));
            }
        }

        if(strengthSum > 0) {
            r = (int)Math.sqrt( r / strengthSum );
            g = (int)Math.sqrt( g / strengthSum );
            b = (int)Math.sqrt( b / strengthSum );
            color = parent.color(r > 255 ? 255 : r, g > 255 ? 255 : g, b > 255 ? 255 : b, getStrength()*255);
        }

        return color;
    }

    public GridElement(PApplet parent, ImpulseSpreader impulseSpreader, PShape shape) {
        this.parent = parent;
        this.impulseSpreader = impulseSpreader;
        neighbours = new ArrayList<>();
        impulses = new ArrayList<>();
        impulsesToBeSpread = new ArrayList<>();
        impulseTimestamps = new HashMap<Impulse, Long>();
        this.timeStamp = System.currentTimeMillis();
        this.shape = shape;
    }

}
