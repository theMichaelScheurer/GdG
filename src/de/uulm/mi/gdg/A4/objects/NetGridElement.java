package de.uulm.mi.gdg.A4.objects;

import processing.core.PApplet;
import processing.core.PShape;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael Scheurer, Philipp Ripper, Simon Ripper
 */
public class NetGridElement extends GridElement{
    private int xStart;
    private int yStart;
    private int yEnd;
    private int xEnd;
    private PApplet parent;
    private long timeStamp;

    public int getxStart() {
		return xStart;
	}

	public int getyStart() {
		return yStart;
	}

	public int getyEnd() {
		return yEnd;
	}

	public int getxEnd() {
		return xEnd;
	}

	public NetGridElement(PApplet parent, ImpulseSpreader impulseSpreader, int xStart, int yStart, int xEnd, int yEnd, PShape shape) {
        super(parent, impulseSpreader, shape);
        this.parent = parent;
        this.xStart = xStart;
        this.xEnd = xEnd;
        this.yEnd = yEnd;
        this.yStart = yStart;
        this.timeStamp = System.currentTimeMillis();
    }

    @Override
    public void draw() {
        parent.pushMatrix();

        parent.stroke(parent.color(255));
        parent.line(xStart, yStart, xEnd, yEnd);
        
        parent.popMatrix();
    }
}
