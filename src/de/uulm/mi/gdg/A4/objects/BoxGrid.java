package de.uulm.mi.gdg.A4.objects;

import de.uulm.mi.gdg.A4.Main;
import de.uulm.mi.gdg.A4.util.ColorPallet;
import processing.core.PApplet;
import processing.core.PShape;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Michael Scheurer
 */
public class BoxGrid extends Grid {

    public enum ElementType {
        BALL, HAXAGON;
    }

    private PApplet parent;
    private ColorPallet colorPallet;

    final float STANDARD_RANDOM_FACTOR = 0.7f;
    final float CLOSER_IMPULSE_FACTOR = 0.9f;

    GridElement[][] elements;

    ArrayList<BoxGridElement> impulseOrigins;

    public BoxGrid(PApplet parent, ColorPallet colorPallet) {
        this.parent = parent;

        Random random = new Random(13374269);

        this.colorPallet = colorPallet;

        ImpulseSpreader impulseSpreader = (impulse, receiver, origin) -> {

            boolean closerToImpulseOrigin = false;

            long impulseAge = System.currentTimeMillis() - impulse.timeStamp;
            float impulseAgeFactor = ((float)impulse.duration - (float)impulseAge) / (float)impulse.duration;
            if(random.nextFloat()  <= (1 * (1-Main.randomImpact)) + ( closerToImpulseOrigin ? CLOSER_IMPULSE_FACTOR*impulseAgeFactor : STANDARD_RANDOM_FACTOR*impulseAgeFactor)*Main.randomImpact)
                receiver.addImpulse(impulse);
        };

        int width = parent.width / 63;

        elements = new GridElement[64][50];
        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {

                PShape shape;

                double edge = width / 1.5;
                double height = Math.sqrt(3) * edge;
                shape = parent.createShape(parent.RECT, i*width, j*width, width-2, width-2);
                shape.setStroke(false);

                elements[i][j] = new BoxGridElement(parent, impulseSpreader, shape);
            }
        }

        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {

                    if(i-1 >= 0 && i-1 < elements.length && j >= 0 && j < elements[i].length)
                        elements[i][j].addNeighbour(elements[i-1][j]);

                    if(i+1 >= 0 && i+1 < elements.length && j >= 0 && j < elements[i].length)
                        elements[i][j].addNeighbour(elements[i+1][j]);

                    if(i >= 0 && i < elements.length && j+1 >= 0 && j+1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i][j+1]);

                    if(i >= 0 && i < elements.length && j-1 >= 0 && j-1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i][j-1]);

            }
        }

        impulseOrigins = new ArrayList<>();
    }

    @Override
    public void update() {
        ArrayList<BoxGridElement> toBeRemoved = new ArrayList<>();
        for(BoxGridElement ge: impulseOrigins) {
            if(ge.impulses.size() <= 0) {
                toBeRemoved.add(ge);
            }
        }
        impulseOrigins.removeAll(toBeRemoved);

        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {
                elements[i][j].update();
            }
        }
    }

    @Override
    public void draw() {
        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {
                elements[i][j].draw();
            }
        }
    }

    @Override
    public void impulse(String gridElementId, Impulse impulse) {

        int x = -1;
        int y = -1;
        int color = parent.color(255, 255, 255);

        if(gridElementId.equals("a1")) {
            x = 3;
            y = 3;
        } else if(gridElementId.equals("b1")) {
            x = 18;
            y = 10;
        } else if(gridElementId.equals("c1")) {
            x = 30;
            y = 18;
        } else if(gridElementId.equals("d1")) {
            x = 38;
            y = 25;
        } else if(gridElementId.equals("e1")) {
            x = 52;
            y = 32;
        }

        switch(gridElementId) {
            case "g1":
                x = 5;
                y = 32;
                color = colorPallet.getValue(1f/18f);
                break;
            case "d2":
                x = 8;
                y = 20;
                color = colorPallet.getValue(2f/18f);
                break;
            case "e2":
                x = 12;
                y = 5;
                color = colorPallet.getValue(3f/18f);
                break;
            case "f#2":
                x = 15;
                y = 20;
                color = colorPallet.getValue(4f/18f);
                break;
            case "g2":
                x = 18;
                y = 32;
                color = colorPallet.getValue(5f/18f);
                break;
            case "a2":
                x = 21;
                y = 20;
                color = colorPallet.getValue(6f/18f);
                break;
            case "b2":
                x = 24;
                y = 5;
                color = colorPallet.getValue(7f/18f);
                break;
            case "c3":
                x = 27;
                y = 20;
                color = colorPallet.getValue(8f/18f);
                break;
            case "d3":
                x = 30;
                y = 32;
                color = colorPallet.getValue(9f/18f);
                break;
            case "e3":
                x = 33;
                y = 20;
                color = colorPallet.getValue(10f/18f);
                break;
            case "f#3":
                x = 36;
                y = 5;
                color = colorPallet.getValue(11f/18f);
                break;
            case "g3":
                x = 39;
                y = 20;
                color = colorPallet.getValue(12f/18f);
                break;
            case "a3":
                x = 42;
                y = 32;
                color = colorPallet.getValue(13f/18f);
                break;
            case "b3":
                x = 45;
                y = 20;
                color = colorPallet.getValue(14f/18f);
                break;
            case "d4":
                x = 48;
                y = 5;
                color = colorPallet.getValue(15f/18f);
                break;
            case "e4":
                x = 51;
                y = 20;
                color = colorPallet.getValue(16f/18f);
                break;
            case "g4":
                x = 54;
                y = 32;
                color = colorPallet.getValue(17f/18f);
                break;
            case "d5":
                x = 57;
                y = 20;
                color = colorPallet.getValue(18f/18f);
                break;
        }

        if(x >= 0 && y >= 0) {
            impulse.color = color;
            impulseOrigins.add((BoxGridElement) elements[x][y]);
            elements[x][y].addImpulse(impulse);
        }
    }
}
