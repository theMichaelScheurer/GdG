package de.uulm.mi.gdg.A4.objects;

import de.uulm.mi.gdg.A4.Main;
import de.uulm.mi.gdg.A4.util.ColorPallet;
import processing.core.PApplet;
import processing.core.PShape;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author Michael Scheurer
 */
public class BallGrid extends Grid {

    public enum ElementType {
        BALL, HAXAGON;
    }

    private PApplet parent;
    private ColorPallet colorPallet;

    final long DELAY = 50;
    final float STANDARD_RANDOM_FACTOR = 0.7f;
    final float CLOSER_IMPULSE_FACTOR = 0.9f;
    final float STANDARD_STRENGHT_REDUCTION = 0.4f;
    final float CLOSER_STRENGTH_REDUCTION = 0.7f;

    GridElement[][] elements;

    ArrayList<BallGridElement> impulseOrigins;

    public BallGrid(PApplet parent, ElementType elementType, ColorPallet colorPallet) {
        this.parent = parent;

        Random random = new Random(13374269);

        this.colorPallet = colorPallet;

        ImpulseSpreader impulseSpreader = (impulse, receiver, origin) -> {

            boolean closerToImpulseOrigin = false;

            for(BallGridElement impulseOrigin : impulseOrigins) {
                //if(!impulseOrigin.impulses.containsKey(impulse.hashString)) {
                    double x = ((BallGridElement)origin).getX() - impulseOrigin.getX();
                    double y = ((BallGridElement)origin).getY() - impulseOrigin.getY();
                    double originDistance = Math.sqrt((x*x + y*y));

                    x = ((BallGridElement)receiver).getX() - impulseOrigin.getX();
                    y = ((BallGridElement)receiver).getY() - impulseOrigin.getY();
                    double receiverDistance = Math.sqrt((x*x + y*y));

                    if (receiverDistance <= originDistance) {
                        closerToImpulseOrigin = true;
                    }
                //}
            }

            long impulseAge = System.currentTimeMillis() - impulse.timeStamp;
            float impulseAgeFactor = ((float)impulse.duration - (float)impulseAge) / (float)impulse.duration;
            if(random.nextFloat()  <= (1 * (1-Main.randomImpact)) + ( closerToImpulseOrigin ? CLOSER_IMPULSE_FACTOR*impulseAgeFactor : STANDARD_RANDOM_FACTOR*impulseAgeFactor)*Main.randomImpact)
                receiver.addImpulse(impulse);
        };

        int width = parent.width / 63;

        elements = new GridElement[64][50];
        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {

                PShape shape;

                double edge = width / 1.5;
                double height = Math.sqrt(3) * edge;

                if (elementType == ElementType.BALL){
                    shape = parent.createShape(parent.ELLIPSE, i*width, j*width + (i%2==0 ? -10 : 0), width-2, width-2);
                    shape.setStroke(false);
                } else {
                    shape = parent.createShape();
                    shape.beginShape();
                    shape.noStroke();
                    shape.vertex(1-(int)(edge/2) + i*width, 1 -(int)(height/2) + j*(int)(height) + (i%2==0 ? -(int)(height/2) : 0));
                    shape.vertex(-1+(int)(edge/2) + i*width, 1 -(int)(height/2) + j*(int)(height) + (i%2==0 ? -(int)(height/2) : 0));
                    shape.vertex(-1+(int) edge + i*width, 0 + j*(int)(height) + (i%2==0 ? -(int)(height/2) : 0));
                    shape.vertex(-1+(int)(edge/2) + i*width, -1 + (int)(height/2) + j*(int)(height) + (i%2==0 ? -(int)(height/2) : 0));
                    shape.vertex(1-(int)(edge/2) + i*width, -1 + (int)(height/2) + j*(int)(height) + (i%2==0 ? -(int)(height/2) : 0));
                    shape.vertex(1-(int)edge + i*width, 0 + j*(int)(height) + (i%2==0 ? -(int)(height/2) : 0));
                    shape.endShape(parent.CLOSE);
                }

                elements[i][j] = new BallGridElement(parent, 16, new PVector(i*width, j*width + (i%2==0 ? -(int)(height/2) : 0)), impulseSpreader, shape);
            }
        }

        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {
                if(i%2==0) {

                    if(i-1 >= 0 && i-1 < elements.length && j >= 0 && j < elements[i].length)
                        elements[i][j].addNeighbour(elements[i-1][j]);

                    if(i-1 >= 0 && i-1 < elements.length && j-1 >= 0 && j-1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i-1][j-1]);

                    if(i+1 >= 0 && i+1 < elements.length && j >= 0 && j < elements[i].length)
                        elements[i][j].addNeighbour(elements[i+1][j]);

                    if(i+1 >= 0 && i+1 < elements.length && j-1 >= 0 && j-1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i+1][j-1]);

                    if(i >= 0 && i < elements.length && j+1 >= 0 && j+1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i][j+1]);

                    if(i >= 0 && i < elements.length && j-1 >= 0 && j-1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i][j-1]);


                } else {
                    if(i-1 >= 0 && i-1 < elements.length && j >= 0 && j < elements[i].length)
                        elements[i][j].addNeighbour(elements[i-1][j]);

                    if(i-1 >= 0 && i-1 < elements.length && j+1 >= 0 && j+1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i-1][j+1]);

                    if(i+1 >= 0 && i+1 < elements.length && j >= 0 && j < elements[i].length)
                        elements[i][j].addNeighbour(elements[i+1][j]);

                    if(i+1 >= 0 && i+1 < elements.length && j+1 >= 0 && j+1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i+1][j+1]);

                    if(i >= 0 && i < elements.length && j+1 >= 0 && j+1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i][j+1]);

                    if(i >= 0 && i < elements.length && j-1 >= 0 && j-1 < elements[i].length)
                        elements[i][j].addNeighbour(elements[i][j-1]);
                }
            }
        }

        impulseOrigins = new ArrayList<>();
    }

    @Override
    public void update() {
        ArrayList<BallGridElement> toBeRemoved = new ArrayList<>();
        for(BallGridElement ge: impulseOrigins) {
            if(ge.impulses.size() <= 0) {
                toBeRemoved.add(ge);
            }
        }
        impulseOrigins.removeAll(toBeRemoved);

        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {
                elements[i][j].update();
            }
        }
    }

    @Override
    public void draw() {
        for(int i = 0; i < elements.length; i++) {
            for(int j = 0; j < elements[i].length; j++) {
                elements[i][j].draw();
            }
        }
    }

    @Override
    public void impulse(String gridElementId, Impulse impulse) {

        int x = -1;
        int y = -1;
        int color = parent.color(255, 255, 255);

        if(gridElementId.equals("a1")) {
            x = 3;
            y = 3;
        } else if(gridElementId.equals("b1")) {
            x = 18;
            y = 10;
        } else if(gridElementId.equals("c1")) {
            x = 30;
            y = 18;
        } else if(gridElementId.equals("d1")) {
            x = 38;
            y = 25;
        } else if(gridElementId.equals("e1")) {
            x = 52;
            y = 32;
        }

        switch(gridElementId) {
            case "g1":
                x = 5;
                y = 28;
                color = colorPallet.getValue(1f/18f);
                break;
            case "d2":
                x = 5;
                y = 25;
                color = colorPallet.getValue(2f/18f);
                break;
            case "e2":
                x = 11;
                y = 25;
                color = colorPallet.getValue(3f/18f);
                break;
            case "f#2":
                x = 11;
                y = 22;
                color = colorPallet.getValue(4f/18f);
                break;
            case "g2":
                x = 17;
                y = 22;
                color = colorPallet.getValue(5f/18f);
                break;
            case "a2":
                x = 17;
                y = 19;
                color = colorPallet.getValue(6f/18f);
                break;
            case "b2":
                x = 23;
                y = 19;
                color = colorPallet.getValue(7f/18f);
                break;
            case "c3":
                x = 23;
                y = 16;
                color = colorPallet.getValue(8f/18f);
                break;
            case "d3":
                x = 29;
                y = 16;
                color = colorPallet.getValue(9f/18f);
                break;
            case "e3":
                x = 29;
                y = 13;
                color = colorPallet.getValue(10f/18f);
                break;
            case "f#3":
                x = 35;
                y = 13;
                color = colorPallet.getValue(11f/18f);
                break;
            case "g3":
                x = 35;
                y = 10;
                color = colorPallet.getValue(12f/18f);
                break;
            case "a3":
                x = 41;
                y = 10;
                color = colorPallet.getValue(13f/18f);
                break;
            case "b3":
                x = 41;
                y = 7;
                color = colorPallet.getValue(14f/18f);
                break;
            case "d4":
                x = 47;
                y = 7;
                color = colorPallet.getValue(15f/18f);
                break;
            case "e4":
                x = 47;
                y = 3;
                color = colorPallet.getValue(16f/18f);
                break;
            case "g4":
                x = 53;
                y = 3;
                color = colorPallet.getValue(17f/18f);
                break;
            case "d5":
                x = 59;
                y = 3;
                color = colorPallet.getValue(18f/18f);
                break;
        }

        if(x >= 0 && y >= 0) {
            impulse.color = color;
            impulseOrigins.add((BallGridElement) elements[x][y]);
            elements[x][y].addImpulse(impulse);
        }
    }
}
