package de.uulm.mi.gdg.A4.objects;

import processing.core.PApplet;
import processing.core.PShape;

public class BoxGridElement extends GridElement {

    public BoxGridElement(PApplet parent, ImpulseSpreader impulseSpreader, PShape shape) {
        super(parent, impulseSpreader, shape);
    }

    @Override
    public void draw() {
        if(this.getStrength() > 0) {
            parent.pushMatrix();


            //parent.fill(parent.color(parent.red(color), parent.green(color), parent.blue(color), strength*255));
            if (getColor() != 0 || getStrength() != 0) {
                System.out.println(getColor());
                System.out.println(getStrength());
                System.out.println(this);
            }

            this.shape.setFill(this.getColor());
            parent.shape(this.shape);

            parent.popMatrix();
        }
    }
}
