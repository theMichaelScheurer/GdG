package de.uulm.mi.gdg.A4.objects;

import processing.core.PApplet;
import processing.core.PShape;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * @author Michael Scheurer
 */
public class BallGridElement extends GridElement {

    private float radius;
    private PVector position;
    private PApplet parent;


    public BallGridElement(PApplet parent, float radius, PVector position, ImpulseSpreader impulseSpreader, PShape shape) {
        super(parent, impulseSpreader, shape);
        this.parent = parent;
        this.radius = radius;
        this.position = position;
    }

    @Override
    public void draw() {
        if(this.getStrength() > 0) {
            parent.pushMatrix();


            //parent.fill(parent.color(parent.red(color), parent.green(color), parent.blue(color), strength*255));
            if (getColor() != 0 || getStrength() != 0) {
                System.out.println(getColor());
                System.out.println(getStrength());
                System.out.println(this);
            }

            this.shape.setFill(this.getColor());
            parent.shape(this.shape);

            parent.popMatrix();
        }
    }

    private int customBlend(int color1, int color2) {

        float r = (float)Math.sqrt( parent.red(color1)*parent.red(color1)+parent.red(color2)*parent.red(color2) / 2 );
        float g = (float)Math.sqrt( parent.green(color1)*parent.green(color1)+parent.green(color2)*parent.green(color2) / 2 );
        float b = (float)Math.sqrt( parent.blue(color1)*parent.blue(color1)+parent.blue(color2)*parent.blue(color2) / 2 );
        return parent.color(r, g, b);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return  position.y;
    }
}


