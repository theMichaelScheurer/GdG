package de.uulm.mi.gdg.A4.objects;

/**
 * @author Michael Scheurer, Philipp Ripper, Simon Ripper
 */
public abstract class Grid {

    public abstract void update();

    public abstract void draw();

    public abstract void impulse(String gridElementId, Impulse impulse);
}
