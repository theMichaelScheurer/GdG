package de.uulm.mi.gdg.A4.objects;

/**
 * @author Michael Scheurer, Philipp Ripper, Simon Ripper
 */
public interface ImpulseSpreader {

    public void accept(Impulse impulse, GridElement receiver, GridElement origin);

}
