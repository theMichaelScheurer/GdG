package de.uulm.mi.gdg.A4;

import ddf.minim.AudioPlayer;
import ddf.minim.Minim;
import ddf.minim.analysis.FFT;
import de.uulm.mi.gdg.A4.objects.*;
import de.uulm.mi.gdg.A4.util.ColorPallet;
import processing.core.PApplet;
import processing.data.JSONArray;
import processing.data.JSONObject;

import controlP5.*;
import java.util.ArrayList;


/**
 * @author Michael Scheurer, Philipp Ripper, Simon Ripper
 */
public class Main extends PApplet {
	
	/**
	 * Variable Windowsize
	 */
	private int windowwidth = 1920;
	private int windowheight = 1080;
	
    private AudioPlayer song;
    private FFT fft;

    final float STRENGTH = 1.0f;
    final long DURATION = 6000;
    final long FADE_IN = 200;
    final long FADE_OUT = 500;
    long DELAY = 200;

    final float RANDOM_FACTOR = 0.5f;
    final float STRENGHT_REDUCTION = 0.85f;
    public static float randomImpact;

    private ArrayList<AnimationTimer> animationTimers;

    ControlP5 controlP5;

    Slider ausbreitungsSlider;
    Slider randomSlider;

    RadioButton gridRadio;
    RadioButton colorRadio;
    
    Grid grid;

    ColorPallet selectedCp;

    ColorPallet cp1;
    ColorPallet cp2;
    ColorPallet cp3;
    ColorPallet cp4;

    @Override
    public void settings(){
        setSize(windowwidth, windowheight);
//        fullScreen();
    }

    @Override
    public void setup() {
        frameRate(120);
        background(0);

        //create player for song
        Minim minim = new Minim(this);
        song = minim.loadFile("res/atlantis.mp3");
        fft = new FFT(song.bufferSize(), song.sampleRate());

        //load timing from json
        loadAnimation();

        /**
         * Buttons for the play/pause and reset
         */
        controlP5 = new ControlP5(this);
        controlP5.addButton("Play/Pause")
        	.setPosition(windowwidth / 2 -100, windowheight / 2 -150)
        	.setSize(200, 30)
        	.plugTo(this, "PlayPause");
        controlP5.addButton("Reset")
    	.setPosition(windowwidth / 2 -100, windowheight / 2 -100)
    	.setSize(200, 30)
    	.plugTo(this, "Reset");

        gridRadio = controlP5.addRadioButton("Gittertyp")
        .setPosition(windowwidth / 2 -100, windowheight / 2 -50)
        .setSize(30, 30)
        .setItemsPerRow(3)
        .setSpacingColumn(40)
        .addItem("Kreise", 0)
        .addItem("Hexagon", 1)
        .addItem("Quadrate", 2)
        .activate(1);

        controlP5.addLabel("Ausbreitungsgeschwindigkeit")
        .setPosition(windowwidth / 2 -100, windowheight / 2)
        .setSize(200, 30);

        ausbreitungsSlider = controlP5.addSlider("", 50, 200)
        .setPosition(windowwidth / 2 -100, windowheight / 2 + 20)
        .setSize(200, 30)
        .plugTo(this, "SetAusbreitungsValue");

        ausbreitungsSlider.setValue(150);

        controlP5.addLabel("Zufaellige Verteilung")
                .setPosition(windowwidth / 2 -100, windowheight / 2 + 70 )
                .setSize(200, 30);

        randomSlider = controlP5.addSlider(" ", 0, 1)
        .setPosition(windowwidth / 2 -100, windowheight / 2 + 90)
        .setSize(200, 30)
        .plugTo(this, "SetRandomValue");

        randomSlider.setValue(0.7f);

        controlP5.addLabel("Farbpalette")
        .setPosition(windowwidth / 2 -100, windowheight / 2 +140)
        .setSize(200, 30);

        colorRadio = controlP5.addRadioButton("Farbwahl")
        .setPosition(windowwidth / 2 -100, windowheight / 2 +160)
        .setSize(30, 30)
        .setItemsPerRow(2)
        .setSpacingColumn(70)
        .setSpacingRow(10)
        .addItem("white", 0)
        .addItem("Red To Blue", 1)
        .addItem("Forest", 2)
        .addItem("Aqua", 3)
        .activate(1)
        .plugTo(this, "SetColorPalette");

        colorRadio.setValue(1);

        cp1 = new ColorPallet(this, color(255));
        cp2 = new ColorPallet(this, color(255, 0, 4), color(232, 0, 174), color(209, 0, 255), color(123, 12, 232), color(58, 13, 255));
        cp3 = new ColorPallet(this, color(255, 103, 0), color(15, 178, 1), color(32, 2, 255));
        cp4 = new ColorPallet(this, color(0, 98, 127), color(0, 196, 255));
        selectedCp = cp1;

        grid = new BallGrid(this, BallGrid.ElementType.BALL, cp1);

//        grid = new HexGrid(this);

//        grid = new NetGrid(this, (impulse, receiver, origin, depth) -> {
//            if(depth < 6)
//                receiver.addImpulse(new Impulse(impulse.strength*STRENGHT_REDUCTION, impulse.color, impulse.duration, impulse.delay+DELAY, impulse.fadeIn, impulse.fadeOut), ++depth);
//        }, 40, 40, 310, 180);
//        grid2 = new NetGrid(this, (impulse, receiver, origin, depth) -> {
//            if(depth < 6)
//                receiver.addImpulse(new Impulse(impulse.strength*STRENGHT_REDUCTION, impulse.color, impulse.duration, impulse.delay+DELAY, impulse.fadeIn, impulse.fadeOut), ++depth);
//        }, 80, 20, 930, 540);
    }

    @Override
    public void draw() {

        while (animationTimers.size() > 0 && animationTimers.get(0).start <= song.position()) {
            AnimationTimer a = animationTimers.remove(0);
            grid.impulse(a.note, new Impulse(a.strength, color(255, 255, 255), a.duration, DELAY, a.fadeIn, a.fadeOut));
        }

        grid.update();
        background(0);
        grid.draw();
    }
    
    /**
     * Function for the Play/Pause-Buttons
     */
    public void PlayPause() {
    	if(!song.isPlaying()) {

            if(colorRadio.getState(0)) {
                selectedCp = cp1;
            } else if(colorRadio.getState(1)) {
                selectedCp = cp2;
            }  else if(colorRadio.getState(2)) {
                selectedCp = cp3;
            }  else if(colorRadio.getState(3)) {
                selectedCp = cp4;
            }

            if(gridRadio.getState(2)) {
                grid = new BoxGrid(this, selectedCp);
            } else if(gridRadio.getState(1)) {
                grid = new BallGrid(this, BallGrid.ElementType.HAXAGON, selectedCp);
            } else {
                grid = new BallGrid(this, BallGrid.ElementType.BALL, selectedCp);
            }

            song.play();
        } else {
            song.pause();
        }
        controlP5.hide();
    }
    
    /***
     * Function for the Reset-Button
     */
    public void Reset() {
    	song.pause();
        song.cue(0);
        loadAnimation();
        controlP5.show();
    }

    public void SetAusbreitungsValue() {
        DELAY = (long)ausbreitungsSlider.getValue();
    }

    public void SetRandomValue() {
        randomImpact = randomSlider.getValue();
    }

    public void SetColorPalette() {
        if(colorRadio.getState(0)) {
            selectedCp = cp1;
        } else if(colorRadio.getState(1)) {
            selectedCp = cp2;
        }  else if(colorRadio.getState(2)) {
            selectedCp = cp3;
        }  else if(colorRadio.getState(3)) {
            selectedCp = cp4;
        }
    }

    @Override
    public void keyPressed() {
        if (key == 'p' || key == 'P') {
        	//play + hide menu and pause + show menu
            if(!song.isPlaying()) {
                song.play();
                controlP5.hide();
            } else {
                song.pause();
                controlP5.show();
            }
        } else if (key == 'm' || key == 'M') {
        	//show or hide menu
    	    if(controlP5.isVisible()) {
                controlP5.hide();
            } else {
                controlP5.show();
            }
    	} else if (key == 'r' || key == 'R') {
    		//reset song and show menu
            song.pause();
            song.cue(0);
            loadAnimation();
            controlP5.show();

        }
    }

    public static void main(String[] args) {
        PApplet.main(new String[]{Main.class.getName()});
    }
    
    private void loadAnimation() {
        //import impulse timing from json
        JSONObject file = loadJSONObject("res/timing.json");
        JSONArray jsonArray = file.getJSONArray("impulses");

        animationTimers = new ArrayList<>();

        for(int i = 0; i < jsonArray.size(); i++) {
            JSONObject o = jsonArray.getJSONObject(i);
            long start = o.getLong("start");
            long duration = o.getLong("duration");
            float strength = o.getFloat("strength");
            long fadeIn = o.getLong("fadeIn");
            long fadeOut = o.getLong("fadeOut");
            String note = o.getString("note");
            animationTimers.add(new AnimationTimer(start, duration, strength, fadeIn, fadeOut, note));
        }

    }

    //class to hold the information for timing an impulse after reading from json.
    private class AnimationTimer {
        long start;
        long duration;
        float strength;
        long fadeIn;
        long fadeOut;
        String note;

        public AnimationTimer(long start, long duration, float strength, long fadeIn, long fadeOut, String note) {
            this.start = start;
            this.duration = duration;
            this.strength = strength;
            this.fadeIn = fadeIn;
            this.fadeOut = fadeOut;
            this.note = note;
        }
    }
}
