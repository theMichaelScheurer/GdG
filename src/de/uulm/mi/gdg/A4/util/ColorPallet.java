package de.uulm.mi.gdg.A4.util;


import processing.core.PApplet;

import java.util.ArrayList;
import java.util.List;

public class ColorPallet {

    private PApplet parent;
    private List<Integer> colorList;

    public ColorPallet(PApplet parent, int... colors) {
        this.parent = parent;

        colorList = new ArrayList<>();
        for(int c : colors) {
            colorList.add(c);
        }
    }

    /**
     * Returns the value on a linear interpolated color pallet.
     *
     * @param interval a float value between 0 and 1.
     * @return
     */
    public int getValue(float interval) {
        if(colorList.size() == 1) {
            return colorList.get(0);
        }

        float val = (interval / (1.0f / ((float)colorList.size()-1)));
        int index = (int)val;

        if(index >= colorList.size()-1) {
            return colorList.get(colorList.size()-1);
        }

        float amount = val - index;
        return parent.lerpColor(colorList.get(index), colorList. get(index+1), amount);
    }

}
